{  
   "wordBaseClass":1,
   "wordClassName":"traitReligious",
   "displayAllWords":false,
   "ordered":false,
   "words":[
	"Religious",
	"worshipping",
	"Worshipper",
	"preaching",
	"putting up fish everywhere",
	"performing makeshift exorcisms",
	"displaying an incredible love of Jesus",
	"doing charity work instead of office work",
	"peforming inpromptu nativity plays",
	"peforming tricks with bread and fish",
	"going to church",
	"forgiving people",
	"being overly nice"
   ],
   "similarListNames":[  

   ],
   "oppositeListNames":[  

   ],
   "modifiers":[
   ]
}