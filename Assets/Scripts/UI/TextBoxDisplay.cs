﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorIndexData
{
    public string colorHex;
    public int character = 10;
    public float delay = 0;

    public ColorIndexData(Color color, int character, float delay)
    {
        this.colorHex = ColorToHex (color) +"ff";
        this.character = character;
        this.delay = delay;
    }

    public ColorIndexData (string colorHex, int character, float delay)
    {
        this.colorHex = colorHex;
        this.character = character;
        this.delay = delay;
    }

    // Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
    public string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
}

public class TextBoxDisplay : MonoBehaviour {

    public Text textBoxName;
    public Text textBoxNameOutline;
    public Text textBoxVoice;

    [HideInInspector]
    public bool textDisplaying = false;

    private List<ColorIndexData> buildColorData = new List<ColorIndexData>();
    private string buildString = "";

    /*
    private void Start()
    {
        BuildText("Hello ", Color.white,0);
        BuildText("Player. \n", Color.cyan,0f);
        BuildText("How are you doing ", Color.white, 1f);
        BuildText("Today? ", Color.red, 0);
        FlushText(1f/20f);
    }
    */

    public void SetName(string text)
    {
        textBoxName.text = text;
    }

    public void BuildText(string text, string colorHex, float delay)
    {
        buildColorData.Add(new ColorIndexData(colorHex, buildString.Length, delay));
        buildString += text;
    }

    public void BuildText(string text, Color color, float delay)
    {   
        buildColorData.Add(new ColorIndexData(color, buildString.Length, delay));
        buildString += text;
    }

    public void FlushText()
    {
        DisplayText(buildString, 1f/60f, buildColorData);
        buildString = "";
        buildColorData = new List<ColorIndexData>();
        
    }

    public void FlushText (float displayDelay)
    {
        DisplayText(buildString, displayDelay, buildColorData);
        buildString = "";
        buildColorData = new List<ColorIndexData>();
    }

    public void DisplayText (string text, float displayDelay, List<ColorIndexData> colorData)
    {
        //check string for tags, and parse
        StopAllCoroutines();
        StartCoroutine(DisplayTextCoroutine(text, displayDelay, colorData));
    }

    private IEnumerator DisplayTextCoroutine (string text, float displayDelay, List<ColorIndexData> colorData)
    {
        textDisplaying = true;

        int character = 0;
        
        string currentString = "<color=#ffffffff>";
        string outlineString = "";

        while (character < text.Length)
        {
            string hide = text.Substring(character);
            
            foreach (ColorIndexData cid in colorData)
            {
                if (cid.character == character)
                {
                    currentString += "</color><color=#" + cid.colorHex + ">";
                    yield return new WaitForSeconds(cid.delay);
                }
            }

            currentString += text[character].ToString();
            outlineString += text[character].ToString();
            
            textBoxVoice.text = currentString + "</color>";
            textBoxVoice.text += "<color=#00000000>";
            textBoxVoice.text += hide;
            textBoxVoice.text += "</color>";

            if (textBoxNameOutline != null)
            {
                textBoxNameOutline.text = outlineString;
                textBoxNameOutline.text += "<color=#00000000>";
                textBoxNameOutline.text += hide;
                textBoxNameOutline.text += "</color>";
            }
            yield return new WaitForSeconds(displayDelay);

            character++;

        }

        textDisplaying = false;
    }
}
