﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioGenerator : MonoBehaviour {

    public WordListManager manager;

    public List<CharacterData> charactersAll = new List<CharacterData>();
    public List<CharacterData> charactersNew = new List<CharacterData>();

    public CharacterData characterPlayer;
    public CharacterData characterBlame;
    public CharacterData characterPicked;
    public TextBoxDisplay textBoxDisplay;

    public string roleAction = "";
    public string traitAction = "";

    public int pickedCharacter = 0;

    public bool next = false;

    public GameMan gameMan;

    public SoundManager soundMan;
    
    public void StartNew()
    {
        StartCoroutine(NewScenario());
    }

    public void SetUp()
    {
        characterPlayer = new CharacterData(manager);
    }

    public IEnumerator NewScenario ()
    {
        
        yield return new WaitForEndOfFrame();
        
        AddCharacters(4);
        pickedCharacter = Random.Range(0, charactersNew.Count);
        characterBlame = charactersNew[pickedCharacter];
        roleAction = characterBlame.GetRoleAction();
        traitAction = characterBlame.GetTraitAction();

    yield return new WaitForEndOfFrame();
        
        Introduction();

        next = false;
        while (next == false)
            yield return new WaitForEndOfFrame();

        DescribeIncident();

        next = false;
        while (next == false)
            yield return new WaitForEndOfFrame();

        PlayerExcuse();

        next = false;
        while (next == false)
            yield return new WaitForEndOfFrame();
        foreach (CharacterData cData in charactersNew)
        {
            DescribeEmployees(cData);
            next = false;
            while (next == false)
                yield return new WaitForEndOfFrame();
        }

        //Accuse
        AccuseCharacterPick();

        gameMan.Pick();

        while (gameMan.picked == false)
            yield return new WaitForEndOfFrame();

        AccuseCharacterExplainOne(characterPicked);
        next = false;
        while (next == false)
            yield return new WaitForEndOfFrame();

        for (int i = 0; i < Random.Range(1, 3); i++)
        {
            AccuseCharacterExplainTwo(characterPicked);

            yield return new WaitForEndOfFrame(); next = false;
            while (next == false)
                yield return new WaitForEndOfFrame();
        }
        

        BossAccuseResponse(characterPicked);




        next = false;
        while (next == false)
            yield return new WaitForEndOfFrame();

        if (gameMan.won)
            WinDialogue();
        else
            LoseDialogue();

        next = false;
        while (next == false)
            yield return new WaitForEndOfFrame();
        gameMan.Results();
        
        next = false;
        while (next == false)
            yield return new WaitForEndOfFrame();

        if (!gameMan.won)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
        else
        {
            gameMan.ShowNextDay();
            textBoxDisplay.BuildText("    ", Color.white, 0);
            textBoxDisplay.FlushText();

            next = false;
            while (next == false)
                yield return new WaitForEndOfFrame();
            gameMan.Reset();
        }
    }

    public void Introduction()
    {
        soundMan.PlaySound(SoundManager.SoundType.Accusation);

        gameMan.SwitchCameraBoss();
        textBoxDisplay.SetName("Big Boss");

        for (int i = 0; i < 2; i++)
        {
            if (Random.Range(0, 10) > 7)
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExclamation") + ", ", Color.white, 0);

            textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ", ", Color.white, 0.5f);

            if (Random.Range(0, 10) > 5)
            {
                textBoxDisplay.BuildText(characterPlayer.name + ", ", Color.green, 0);
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ". \n", Color.white, 0.5f);
            }
            else
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExclamation") + ". ", Color.white, 0.5f);

            if (Random.Range(0, 10) > 7)
            {
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ". ", Color.white, 0.5f);
                textBoxDisplay.BuildText(characterPlayer.name + ". \n", Color.green, 0);
            }
        }

        textBoxDisplay.FlushText();
    }

    public void DescribeIncident()
    {

        gameMan.SwitchCameraBoss();
        soundMan.PlaySound(SoundManager.SoundType.Accusation);
        textBoxDisplay.SetName("Big Boss");

        //I'd expect that from
        //

        textBoxDisplay.BuildText(manager.GetWordRng("adjectiveSize", 0) + " " + roleAction + " ", Color.red, 0f);
        textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ". \n", Color.white, 0f);
        textBoxDisplay.BuildText(manager.GetWordRng("adjectiveSize", 0) + " " + traitAction + "\n", Color.red, 1f);
        textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExclamation") + " ", Color.white, 1f);
        textBoxDisplay.BuildText(characterPlayer.name + ". \n", Color.green, 0.5f);
        textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ". \n", Color.white, 0f);
        textBoxDisplay.FlushText(1f / 40f);
    }
    
    public void DescribeEmployees(CharacterData character)
    {

        gameMan.SwitchCameraBoss();
        soundMan.PlaySound(SoundManager.SoundType.Pose);
        textBoxDisplay.SetName("Big Boss");
        for (int i = 0; i < Random.Range(1, 2);i++)
        {

            if (Random.Range(0, 10) > 6)
            {
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExpectation"), Color.white, 0f);
                textBoxDisplay.BuildText(" " + character.nameWithTrait, Color.yellow, 0f);
                textBoxDisplay.BuildText(" from ", Color.white, 0f);

                textBoxDisplay.BuildText(" " + character.role, Color.blue, 0f);

            }
            else
            {
                textBoxDisplay.BuildText(character.nameWithTrait, Color.yellow, 0f);
                textBoxDisplay.BuildText(" from ", Color.white, 0f);

                textBoxDisplay.BuildText(" " + character.role + " ", Color.blue, 0f);
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechCharacterIntros"), Color.white, 0f);

                if (Random.Range(0, 10) > 3)
                {
                    textBoxDisplay.BuildText(" and " + manager.GetWordRng("bossSpeechCharacterIntros") + ".", Color.white, 0.5f);
                }
            }
            textBoxDisplay.BuildText("\n", Color.white, 1f);
        }
        

        textBoxDisplay.FlushText(1f/40f);
    }

    public void PlayerExcuse()
    {

        gameMan.SwitchCameraPlayer();
        soundMan.PlaySound(SoundManager.SoundType.Excuse);
        textBoxDisplay.SetName(characterPlayer.name);

        textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechExplain") + " and ", Color.white, 0.5f);
        textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechExplain") + ".", Color.white, 0.5f);
        
        textBoxDisplay.FlushText();
    }


    public void AccusePlayer()
    {

        gameMan.SwitchCameraPlayer();
        soundMan.PlaySound(SoundManager.SoundType.Accusation);
        textBoxDisplay.SetName("Big Boss");

        for (int i = 0; i < 2; i++)
        {
            if (Random.Range(0, 10) > 7)
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExclamation") + ", ", Color.white, 0);

            textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ", ", Color.white, 0.5f);

            if (Random.Range(0, 10) > 5)
            {
                textBoxDisplay.BuildText(characterPlayer.name + ", ", Color.green, 0);
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ". \n", Color.white, 0.5f);
            }
            else
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExclamation") + ". ", Color.white, 0.5f);

            if (Random.Range(0, 10) > 7)
            {
                textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechExposition") + ". ", Color.white, 0.5f);
                textBoxDisplay.BuildText(characterPlayer.name + ". \n", Color.green, 0);
            }
        }


        textBoxDisplay.FlushText();
    }

    public void AccuseCharacterPick()
    {

        gameMan.SwitchCameraPlayer();
        soundMan.PlaySound(SoundManager.SoundType.Excuse);

        textBoxDisplay.SetName(characterPlayer.name);

        textBoxDisplay.BuildText(traitAction, Color.red, 0f);
        textBoxDisplay.BuildText(" and ", Color.white, 0f);
        textBoxDisplay.BuildText(roleAction + "\n", Color.red, 0f);
        textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechExplain") + ", ", Color.white, 0.5f);
        textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechExplain") + "? \n", Color.white, 0.5f);
        textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechAccuse") + "... ", Color.white, 0.5f);

        textBoxDisplay.FlushText();
    }

    public void AccuseCharacterExplainOne(CharacterData character)
    {
        gameMan.SwitchCameraPlayer();
        soundMan.PlaySound(SoundManager.SoundType.Excuse);

        textBoxDisplay.SetName(characterPlayer.name);

        textBoxDisplay.BuildText(traitAction, Color.red, 0f);
        textBoxDisplay.BuildText(" and ", Color.white, 0f);
        textBoxDisplay.BuildText(roleAction + "\n", Color.red, 0f);
        textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechAccuse") + " ", Color.white, 0.5f);
        textBoxDisplay.BuildText(character.nameWithTrait + ".", Color.yellow, 0);

        textBoxDisplay.FlushText();
    }

    public void AccuseCharacterExplainTwo(CharacterData character)
    {
        gameMan.SwitchCameraPlayer();
        soundMan.PlaySound(SoundManager.SoundType.Excuse);

        textBoxDisplay.SetName(characterPlayer.name);

        for (int i = 0; i < Random.Range(3,5); i++)
        {
            if (Random.Range(0, 10) > 7)
            {
                textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechAccuse") + " ", Color.white, 0.5f);
                textBoxDisplay.BuildText(character.nameWithTrait + ", ", Color.yellow, 0);
            }
            else
            {
                textBoxDisplay.BuildText(character.nameWithTrait + " ", Color.yellow, 0);
                textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechAccuseTwo") + " ", Color.white, 0.5f);
                if (Random.Range(0, 10) > 7)
                {
                    textBoxDisplay.BuildText(" and " + manager.GetWordRng("playerSpeechAccuseTwo") + " ", Color.white, 0.5f);
                }
            }
        }
        textBoxDisplay.FlushText();
    }

    public void BossAccuseResponse (CharacterData character)
    {
        gameMan.SwitchCameraBoss();
        soundMan.PlaySound(SoundManager.SoundType.Deliberation);

        textBoxDisplay.SetName("Big Boss");
        textBoxDisplay.BuildText("You say ", Color.white, 0.5f);
        textBoxDisplay.BuildText(character.nameWithTrait + " ", Color.yellow, 0.5f);
        textBoxDisplay.BuildText(manager.GetWordRng("playerSpeechAccuseTwo") + ", eh ", Color.white, 0.5f);
        textBoxDisplay.BuildText(characterPlayer.name + " ?", Color.green, 0.5f);
        textBoxDisplay.FlushText();
    }

    public void WinDialogue()
    {

        gameMan.SwitchCameraBoss();
        soundMan.PlaySound(SoundManager.SoundType.Deliberation);

        textBoxDisplay.SetName("Big Boss");
        textBoxDisplay.BuildText(characterPlayer.name + " ", Color.green, 0.5f);

        for (int i = 0; i < Random.Range(2, 4); i++)
        {
            textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechAgree") + ", ", Color.white, 0.5f);
        }
        textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechAgree") + ".", Color.white, 0.5f);
        textBoxDisplay.FlushText();
    }

    public void LoseDialogue()
    {

        gameMan.SwitchCameraBoss();
        soundMan.PlaySound(SoundManager.SoundType.Deliberation);

        textBoxDisplay.SetName("Big Boss");
        textBoxDisplay.BuildText(characterPlayer.name + " ", Color.green, 0.5f);

        for (int i = 0; i < Random.Range(2, 4); i++)
        {
            textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechDisagree") + ", ", Color.white, 0.5f);
        }
        textBoxDisplay.BuildText(manager.GetWordRng("bossSpeechDisagree") + ".", Color.white, 0.5f);
        textBoxDisplay.FlushText();
    }
    public void AddCharacters (int count) {
        charactersNew.Clear();

        for (int i = 0; i < count; i++)
        {
            charactersNew.Add(new CharacterData(manager));
        }
        charactersAll.AddRange(charactersNew);
    }
	



}
