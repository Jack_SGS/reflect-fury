﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterData : MonoBehaviour {

    private WordListManager manager;

    //public int traitIndex = 0;
    //public int roleIndex = 0;

    public string name = "";
    public string nameWithTrait = "";

    public string traitWordListName = "";
    public string trait = "";
    public string traitNoun = "";
    public string traitInsult = "";

    public string roleWordListName = "";
    public string role = "";
    public string roleNoun = "";
    public string roleInsult = "";

    /*
    public int heightIndex = 0;
    public string height = "";
    */

    public CharacterData(WordListManager manager)
    {
        this.manager = manager;
        name = manager.GetWordRng("names");

        WordList roleList = manager.wordListDict["jobRoles"];
        WordList traitList = manager.wordListDict["characterTraits"];
        
        traitWordListName = "trait" + traitList.GetRandomWord();
        roleWordListName = "role" + roleList.GetRandomWord();

        trait       = manager.GetWord(traitWordListName, 0, -1);
        traitNoun   = manager.GetWord(traitWordListName, 1, -1);
        traitInsult = manager.GetWord(traitWordListName, 2, -1);

        role        = manager.GetWord(roleWordListName, 0, -1);
        roleNoun    = manager.GetWord(roleWordListName, 1, -1);
        roleInsult  = manager.GetWord(roleWordListName, 2, -1);

        nameWithTrait = trait + " " + name;
    }

    public string GetTraitAction()
    {
        int index = Random.Range(3, manager.wordListDict["characterTraits"].words.Length);

        return manager.GetWord(traitWordListName, index, -1);
    }

    public string GetRoleAction()
    {
        int index = Random.Range(3, manager.wordListDict["jobRoles"].words.Length);

        return manager.GetWord(roleWordListName, index, -1);
    }
}