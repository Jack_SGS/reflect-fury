﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FadePanel : MonoBehaviour, IPointerClickHandler
{

    public Image panelImage;
    public delegate void FadeOutCallback();
    public Game game;

    private FadeOutCallback callback;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void FadeIn(FadeOutCallback newCallback = null)
    {
        iTween.Stop();
        this.callback = newCallback;
        iTween.ValueTo(this.gameObject, iTween.Hash("from", 1.0f, "to", 0.0f, "time", 1.0f, "onupdate", "UpdateAlphaValue", "oncomplete", "FinishedFadeIn"));
    }

    public void FinishedFadeIn()
    {
        if(this.callback != null)
        {
            this.callback();
        }
    }

    public void FadeOut(FadeOutCallback newCallback = null)
    {
        iTween.Stop();
        this.callback = newCallback;
        iTween.ValueTo(this.gameObject, iTween.Hash("from", 0f, "to", 1f, "time", 1.0f, "onupdate", "UpdateAlphaValue", "oncomplete", "FinishedFadeOut"));
    }

    public void FinishedFadeOut()
    {
        if (this.callback != null)
        {
            this.callback();
        }
    }

    public void UpdateAlphaValue(float newAlpha)
    {
        Color color = panelImage.color;
        color.a = newAlpha;
        panelImage.color = color;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(game != null)
        {
            game.PanelClickCallback();
        }
    }
}
