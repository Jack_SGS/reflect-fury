﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioSource sfxSource, bossAngryLoopSource, playerWorriedLoopSource;

    public enum SoundType
    {
        Accusation, Deliberation, Verdict, Celebration, Defeat, Excuse, Pose, PoseFinal
    };

    public AudioClip[] accusationArray, deliberationArray, verdictArray, celebrationArray, defeatArray, excuseArray,
        poseArray, poseFinalArray;
    private List<AudioClip> accusationList, deliberatonList, verdictList, celebrationList, defeatList, excuseList,
        poseList, poseFinalList;

    private Dictionary<SoundType, List<AudioClip>> typeToClip;
    private Dictionary<SoundType, AudioClip[]> typeToArray;

	// Use this for initialization
	void Start () {

        typeToClip = new Dictionary<SoundType, List<AudioClip>>();
        typeToArray = new Dictionary<SoundType, AudioClip[]>();

        accusationList = new List<AudioClip>(accusationArray);
        deliberatonList = new List<AudioClip>(deliberationArray);
        verdictList = new List<AudioClip>(verdictArray);
        celebrationList = new List<AudioClip>(celebrationArray);
        defeatList = new List<AudioClip>(defeatArray);
        excuseList = new List<AudioClip>(excuseArray);
        poseList = new List<AudioClip>(poseArray);
        poseFinalList = new List<AudioClip>(poseFinalArray);

        typeToClip.Add(SoundType.Accusation, accusationList);
        typeToClip.Add(SoundType.Deliberation, deliberatonList);
        typeToClip.Add(SoundType.Verdict, verdictList);
        typeToClip.Add(SoundType.Celebration, celebrationList);
        typeToClip.Add(SoundType.Defeat, defeatList);
        typeToClip.Add(SoundType.Excuse, excuseList);
        typeToClip.Add(SoundType.Pose, poseList);
        typeToClip.Add(SoundType.PoseFinal, poseFinalList);

        typeToArray.Add(SoundType.Accusation, accusationArray);
        typeToArray.Add(SoundType.Deliberation, deliberationArray);
        typeToArray.Add(SoundType.Verdict, verdictArray);
        typeToArray.Add(SoundType.Celebration, celebrationArray);
        typeToArray.Add(SoundType.Defeat, defeatArray);
        typeToArray.Add(SoundType.Excuse, excuseArray);
        typeToArray.Add(SoundType.Pose, poseArray);
        typeToArray.Add(SoundType.PoseFinal, poseFinalArray);

        bossAngryLoopSource.Play();
        bossAngryLoopSource.Pause();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySound(SoundType type)
    {
        List<AudioClip> list = typeToClip[type];

        int randomNumber = Random.Range(0, list.Count);
        sfxSource.PlayOneShot(list[randomNumber]);
        list.RemoveAt(randomNumber);

        if (list.Count < 1)
        {
            list.AddRange(typeToArray[type]);
        }
    }

    public void SetBossAngryLoopPaused(bool paused)
    {
        if (paused)
        {
            bossAngryLoopSource.Pause();
        }
        else
        {
            bossAngryLoopSource.UnPause();
        }
    }

}
