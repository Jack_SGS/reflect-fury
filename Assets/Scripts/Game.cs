﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour {

    enum Stage
    {
        Generation, StartOfDay, Accusation, Excuse, Deliberation, Verdict, End
    };

    private Stage m_CurrentStage;
    private string m_AccusationString;
    private Camera m_CurrentCamera;
    private GameObject m_CurrentBoss;
    private bool m_ExcuseAccepted, m_ButtonsLocked;

    public Canvas thoughtBubbleCanvas;
    public CanvasGroup thoughtBubbleGroup, thoughtBubbleTextGroup;
    public Text thoughtBubbleText;
    private int m_NameIndex;
    private string[] m_NameArray = new string[] { "Chez", "Geoffrey", "Sarah" };

    public FadePanel fadePanel;
    public Camera[] cameras;

    public SoundManager soundManager;

    public GameObject[] defaultBossArray, deliberationBossArray, accusationBossArray, verdictBossArray;
    private List<GameObject> defaultList, deliberationList, accusationList, verdictList;
    private GameObject currentDefaultBoss, currentDeliberationBoss, currentAccusationBoss, currentVerdictBoss;

    public int winCount;

    // Use this for initialization
    void Start () {

        defaultList = new List<GameObject>(defaultBossArray);
        deliberationList = new List<GameObject>(deliberationBossArray);
        accusationList = new List<GameObject>(accusationBossArray);
        verdictList = new List<GameObject>(verdictBossArray);

        GenerateOfficeData();
        BeginStage();
	}

    private void BeginStage()
    {
        switch (m_CurrentStage)
        {
            case (Stage.Generation):
                SwitchBoss("Default");
                m_ExcuseAccepted = false;
                m_ButtonsLocked = false;
                m_NameIndex = 0;
                fadePanel.FadeIn(new FadePanel.FadeOutCallback(GenerationFadeCallback));
                break;
            case (Stage.StartOfDay):
                m_AccusationString = GenerateAccusation();
                m_CurrentStage = Stage.Accusation;
                BeginStage();
                break;
            case (Stage.Accusation):
                SwitchBoss("Accusation");
                SwitchCamera("Accusation");
                soundManager.PlaySound(SoundManager.SoundType.Accusation);
                break;
            case (Stage.Excuse):
                SwitchCamera("Excuse");
                SwitchBoss("Default");
                soundManager.PlaySound(SoundManager.SoundType.Excuse);
                thoughtBubbleText.text = m_NameArray[m_NameIndex];
                StartCoroutine(ExcuseCoroutine());
                break;
            case (Stage.Deliberation):
                SwitchBoss("Deliberation");
                SwitchCamera("Deliberation");
                StartCoroutine(DeliberationCoroutine());
                break;
            case (Stage.Verdict):
                m_ExcuseAccepted = AnalyseExcuse();
                SwitchBoss("Verdict");
                SwitchCamera("Default");
                soundManager.PlaySound(SoundManager.SoundType.PoseFinal);
                soundManager.PlaySound(SoundManager.SoundType.Verdict);
                break;
            case (Stage.End):
                if (m_ExcuseAccepted)
                {
                    //soundManager.PlaySound(SoundManager.SoundType.Celebration);
                }
                else
                {
                    soundManager.PlaySound(SoundManager.SoundType.Defeat);
                }
                fadePanel.FadeOut(new FadePanel.FadeOutCallback(EndFadeCallback));
                break;
        }
    }

    public void GenerationFadeCallback()
    {
        m_CurrentStage = Stage.StartOfDay;
        BeginStage();
    }

    public void EndFadeCallback()
    {
        winCount++;
        m_CurrentStage = Stage.Generation;
        BeginStage();
    }

    public void PanelClickCallback()
    {
        switch (m_CurrentStage)
        {
            case (Stage.Generation):
                break;
            case (Stage.StartOfDay):
                break;
            case (Stage.Accusation):
                m_CurrentStage = Stage.Excuse;
                BeginStage();
                break;
            case (Stage.Excuse):
                // m_CurrentStage = Stage.Deliberation;
                // BeginStage();
                break;
            case (Stage.Deliberation):
                break;
            case (Stage.Verdict):
                m_CurrentStage = Stage.End;
                BeginStage();
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow) && m_CurrentStage == Stage.Excuse)
        {
            ThoughtBubbleButtonClick(true);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && m_CurrentStage == Stage.Excuse)
        {
            ThoughtBubbleButtonClick(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(m_CurrentStage == Stage.Excuse)
            {
                ThoughtBubbleSelection();
            }
            else
            {
                PanelClickCallback();
            }
            
        }
       

        switch (m_CurrentStage)
        {
            case (Stage.Generation):
                UpdateGeneration();
                break;
            case (Stage.StartOfDay):
                UpdateStartOfDay();
                break;
            case (Stage.Accusation):
                UpdateAccusation();
                break;
            case (Stage.Excuse):
                UpdateExcuse();
                break;
            case (Stage.Deliberation):
                UpdateDeliberation();
                break;
            case (Stage.Verdict):
                UpdateVerdict();
                break;
        }
		
	}

    private void UpdateGeneration()
    {
        
    }

    private void UpdateStartOfDay()
    {
        
    }

    private void UpdateAccusation()
    {

    }

    private void UpdateExcuse()
    {

    }

    private void UpdateDeliberation()
    {

    }

    private void UpdateVerdict()
    {

    }

    private void GenerateOfficeData()
    {

    }

    private string GenerateAccusation()
    {
        return ("Sombody carried out the sacrificial slaughter of a goat on the top kitchen shelf.");
    }

    private void SwitchCamera(string cameraTag)
    {
        for(int i = 0; i < cameras.Length; i++)
        {
            if(cameras[i].gameObject.tag == cameraTag + " Camera")
            {
                m_CurrentCamera = cameras[i];
                cameras[i].gameObject.SetActive(true);
            }
            else
            {
                cameras[i].gameObject.SetActive(false);
            }
        }
    }

    private void SwitchBoss(string type)
    {
        int randomNumber;
        GameObject[] array = null;
        List<GameObject> list = null;

        switch (type)
        {
            case ("Default"):
                list = defaultList;
                array = defaultBossArray;
                break;
            case ("Deliberation"):
                list = deliberationList;
                array = deliberationBossArray;
                break;
            case ("Accusation"):
                list = accusationList;
                array = accusationBossArray;
                break;
            case ("Verdict"):
                list = verdictList;
                array = verdictBossArray;
                break;
        }

        if(m_CurrentBoss != null)
        {
            m_CurrentBoss.SetActive(false);
        }

        randomNumber = Random.Range(0, list.Count);
        m_CurrentBoss = list[randomNumber];
        m_CurrentBoss.SetActive(true);
        list.RemoveAt(randomNumber);

        if (list.Count < 1)
        {
            list.AddRange(array);
        }

    }

    private string[][] GenerateThoughts()
    {
        string[][] employeesAndAttributes = new string[2][];
        employeesAndAttributes[0] = new string[] {"Deborah", "Jimmy"};
        employeesAndAttributes[1] = new string[] {"Gluten Intolerent", "HR"};
        return employeesAndAttributes;
    }

    private bool AnalyseExcuse()
    {
        return true;
    }

    private IEnumerator DeliberationCoroutine()
    {

        float originalFOV = m_CurrentCamera.fieldOfView;

        for (int i = 0; i < winCount + 1; i++)
        {
            SwitchBoss("Deliberation");
            soundManager.PlaySound(SoundManager.SoundType.Pose);
            soundManager.PlaySound(SoundManager.SoundType.Deliberation);
            float fov = m_CurrentCamera.fieldOfView;
            fov *= 0.8f;
            m_CurrentCamera.fieldOfView = fov;
            yield return new WaitForSeconds(4.0f);
        }

        m_CurrentCamera.fieldOfView = originalFOV;
        SwitchBoss("Default");
        m_CurrentStage = Stage.Verdict;
        BeginStage();
    }

    private IEnumerator ExcuseCoroutine()
    {
        yield return new WaitForSeconds(1.0f);

        iTween.RotateAdd(m_CurrentCamera.gameObject, new Vector3(-20, 0, 0), 2.0f);

        iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", 1, "time", 2.0f, "onupdate", "BubbleAlphaUpdate"));
    }

    public void BubbleAlphaUpdate(float newAlpha)
    {
        thoughtBubbleGroup.alpha = newAlpha;
    }

    public void ThoughtBubbleButtonClick(bool last)
    {
        if (!m_ButtonsLocked)
        {
            m_ButtonsLocked = true;

            if (last)
            {
                m_NameIndex--;
                if(m_NameIndex < 0)
                {
                    m_NameIndex = m_NameArray.Length - 1;
                }
            }
            else
            {
                m_NameIndex++;
                if(m_NameIndex >= m_NameArray.Length)
                {
                    m_NameIndex = 0;
                }
            }

            StartCoroutine(ThoughtBubbleClickCoroutine());
        }
    }

    public IEnumerator ThoughtBubbleClickCoroutine()
    {

        iTween.ValueTo(this.gameObject, iTween.Hash("from", 1, "to", 0, "time", 0.5f, "onupdate", "BubbleTextAlphaUpdate"));

        yield return new WaitForSeconds(0.5f);

        thoughtBubbleText.text = m_NameArray[m_NameIndex];

        iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", 1, "time", 0.5f, "onupdate", "BubbleTextAlphaUpdate"));

        yield return new WaitForSeconds(0.5f);

        m_ButtonsLocked = false;

    }

    public void BubbleTextAlphaUpdate(float newAlpha)
    {
        thoughtBubbleTextGroup.alpha = newAlpha;
    }

    private void ThoughtBubbleSelection()
    {
        m_ButtonsLocked = true;
        soundManager.PlaySound(SoundManager.SoundType.Excuse);
        iTween.RotateAdd(m_CurrentCamera.gameObject, new Vector3(20, 0, 0), 2.0f);
        iTween.ValueTo(this.gameObject, iTween.Hash("from", 1, "to", 0, "time", 2.0f, "onupdate", "BubbleAlphaUpdate", "oncomplete", "BeginStage"));
        m_CurrentStage = Stage.Deliberation;

    }
}
