﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMan : MonoBehaviour {

    public GameObject cam;
    public List<Transform> playerCamTransforms, bossCamTransforms;

    public ScenarioGenerator scenario;
    public GameObject title, pickVictim, youWin, youLose, nextDay, textPanel;

    public Text[] accuseButtonsText;

    public bool picked = false;
    public bool won = false;


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            scenario.next = true;
        }
    }

    public void SwitchCameraBoss()
    {
        Transform t = bossCamTransforms[Random.Range(0, bossCamTransforms.Count)];
        cam.transform.position = t.transform.position;
        cam.transform.rotation = t.transform.rotation;
    }

    public void SwitchCameraPlayer()
    {
        Transform t = bossCamTransforms[Random.Range(0, playerCamTransforms.Count)];
        cam.transform.position = t.transform.position;
        cam.transform.rotation = t.transform.rotation;
    }

    public void AccuseOption(int pick)
    {
        if (pick == scenario.pickedCharacter)
            won = true;
        else
            won = false;

        scenario.characterPicked = scenario.charactersNew[pick];

        picked = true;

        pickVictim.gameObject.SetActive(false);

    }

    public void Pick()
    {
        for (int i = 0; i < scenario.charactersNew.Count; i++)
        {
            accuseButtonsText[i].text = scenario.charactersNew[i].nameWithTrait + " from " + scenario.charactersNew[i].role;
            pickVictim.gameObject.SetActive(true);
        }
    }


    public void Results()
    {
        textPanel.SetActive(false);
        if (won)
            youWin.SetActive(true);
        else
            youLose.SetActive(true);
    }


    public void ShowNextDay()
    {
        youWin.SetActive(false);
        youLose.SetActive(false);
        nextDay.SetActive(true);
    }

    public void StartNewGame()
    {
        scenario.SetUp();
        Reset();
    }

    public void Reset()
    {
        picked = false;
        won = false;
        nextDay.SetActive(false);
        title.SetActive(false);
        scenario.StartNew ();
        textPanel.SetActive(true);
    }
}
