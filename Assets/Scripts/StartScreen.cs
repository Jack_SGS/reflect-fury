﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour {

    private bool m_Transitioning = false;
    public FadePanel fadePanel;
    public AudioSource startSource;
    public Animator pressSpaceAnimator;

	// Use this for initialization
	void Start () {
        fadePanel.FadeIn();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space) && !m_Transitioning)
        {
            m_Transitioning = true;
            fadePanel.FadeOut(new FadePanel.FadeOutCallback(FadeComplete));
            startSource.Play();
            pressSpaceAnimator.speed = 3;
        }
	}

    public void FadeComplete()
    {
        SceneManager.LoadScene("Game");
    }
}
