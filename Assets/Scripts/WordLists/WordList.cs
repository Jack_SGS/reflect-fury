﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WordList {

    //common is treated as a list of seporate object
    //decending lists are ordered by severity (eg temperature)
    public enum WordBaseClass { Noun, Verb, Adjective, Adverb, Pronouns, Prepositions, Conjunctions, Determiner, Exclamation, Other };
    public WordBaseClass wordBaseClass = WordBaseClass.Other;
    public string wordClassName;

    public bool displayAllWords = false;
    public bool ordered = false;

    public string[] words = new string[] { };
    public string[] similarListNames = new string[] { };
    public string[] oppositeListNames = new string[] { };
    public string[] modifiers = new string[] { };

    public void Serialize ()
    {
        Debug.Log (JsonUtility.ToJson(this));
    }
    
    public string GetWord (int index)
    {
        return words[index];
    }

    public string GetWord(int index, int modifier)
    {
        if (modifier > -1)
            return words[index] + modifiers[modifier];

        return GetWord(index);
    }

    public string GetRandomWord ()
    {
        return words [Random.Range(0, words.Length)];
    }

    public string GetRandomWord(out int randomIndex)
    {
        randomIndex = Random.Range(0, words.Length);
        return words[randomIndex];
    }


    public string GetModifier (int index)
    {
        if (index < 0)
            return "";
        return modifiers[index];
    }
    
}