﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordListManager : MonoBehaviour {

    public static WordListManager instance;

    public string wordListResourcePath = "WordLists/";

    public Dictionary<string, WordList> wordListDict = new Dictionary<string, WordList>();
    public List<WordList> wordLists = new List<WordList>();
    
    private void Awake()
    {
        instance = this;
    }

    void Start () {
        
        float time = Time.realtimeSinceStartup;

        TextAsset[] jsonData = Resources.LoadAll<TextAsset>(wordListResourcePath);

        foreach (TextAsset json in jsonData)
        {
            Debug.Log(json);
            WordList wl = JsonUtility.FromJson<WordList>(json.text);
            wordListDict.Add(wl.wordClassName, wl);
            wordLists.Add (wl);
            Debug.Log("Loaded (" + wl.wordClassName +")");
        }

        Debug.Log("-------- Loaded " + wordLists.Count + " word lists in " + (Time.realtimeSinceStartup - time));
    }

    public string GetWordRng(string wordTypeName)
    {
        return GetWordRng(wordTypeName, -1);
    }

    public string GetWordRng (string wordTypeName, int modiferIndex)
    {
        int rng = 0;
        return GetWordRng (wordTypeName, modiferIndex, out rng);
    }

    public string GetWordRng(string wordTypeName, int modiferIndex, out int pickedIndex)
    {
        string word = "";
        pickedIndex = 0;
        if (wordListDict.ContainsKey(wordTypeName))
        {
            word = wordListDict[wordTypeName].GetRandomWord(out pickedIndex);
            word += wordListDict[wordTypeName].GetModifier(modiferIndex);
        }

        return word;
    }

    public int GetWordIndex(string wordTypeName,string word)
    {
        string[] words = wordListDict[wordTypeName].words;

        for (int i = 0; i < words.Length; i++)
        {
            if (word == words[i])
                return i;

            for (int j = 0; j < wordListDict[wordTypeName].modifiers.Length; i++)
            {
                if (word == words[i] + wordListDict[wordTypeName].modifiers[j])
                    return i;
            }
        }

        return -1;
    }
    
    public string GetWord(string wordStructure, int wordIndex, int modifierIndex)
    {
        return wordListDict[wordStructure].GetWord(wordIndex, modifierIndex);
    }

}
